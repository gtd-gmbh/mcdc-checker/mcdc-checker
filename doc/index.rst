..
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

Welcome to the documentation!
=============================

.. toctree::
   :maxdepth: 2
   :caption: First steps

   usage/installation
   usage/usage
   usage/result.rst

.. toctree::
   :maxdepth: 2
   :caption: Theory of Operation

   theory/operation

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   api/main


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
