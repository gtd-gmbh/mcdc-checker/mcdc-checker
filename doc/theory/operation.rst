..
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

Papers and Presentations
------------------------

The MCDC Checker tool is based on the theory described in the following papers and presentations:

.. topic:: Formalization and Comparison of MCDC and Object Branch Coverage Criteria

    The paper by Cyrille Comar, Jerome Guitton, Olivier Hainque, Thomas Quinot of AdaCore gives a
    formal proof of why object branch coverage and MCDC coverage is equivalent for tree-like binary
    decision diagrams.

    The paper is available here: :download:`Formalization and Comparison of MCDC and Object Branch Coverage Criteria <papers/Couverture_ERTS-2012.pdf>`

.. topic:: An Introduction to Binary Decision Diagrams

   This paper by Henrik Reif Andersen of IT Univesity of Copenhagen gives an overview over what
   binary decision diagrams are and what they can be used for.

   The paper is available here: :download:`An Introduction to Binary Decision Diagrams <papers/andersen-bdd.pdf>`

.. topic:: Efficient Implementation of a BDD package

   This paper by Karl S. Brace, Richard L. Rudell, Randal E. Bryant of Carnegie Mellon and
   Synopsys, Inc. gives an overview over algorithms to implement operations to transform binary decision
   diagrams.

   The paper is available here: :download:`Efficient Implementation of a BDD package <papers/bryant-bdd-1991.pdf>`

.. topic:: MC/DC for Space - A new Approach to Ensure MC/DC Structural Coverage with Exclusively Open Source Tools

   This presentation by Thomas Wucher and Andoni Arregui of GTD GmbH given at DASIA 2021 and ESA
   Software Product Assurance Workshop 2021 explains the basic approach behind the MCDC Checker tool.

   The presentation is available here: `MC/DC for Space <../MCDC_for_Space_ESA_Software_PA_Workshop_2021.pdf>`_
