..
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

Usage
=====

Synopsis
~~~~~~~~
::

    mcdc_checker [-h] [-I INCLUDE [INCLUDE ...]] [-D DEFINE [DEFINE ...]] [-a] [-d] [--] [file]

Arguments
~~~~~~~~~

None of the command line options is strictly required, but either ``-a`` or ``file`` has to be given.

Optional arguments
------------------

.. option:: -I INCLUDE, --include INCLUDE

    Include files passed to the clang preprocessor. Can be specified multiple times.

.. option:: -D DEFINE, --define DEFINE

    Define passed to the clang preprocessor. Can be specified multiple times.

.. option:: -j FILE, --json-output FILE

    Write all errors found during analysis to FILE in JSON format. The JSON produced is
    compliant to the Code Climate JSON report specification.

.. option:: -a, --all

    Check all C/C++ implementation and header files in current directory recursively.
    With this option, all files with the following file extensions are checked:

        c, cc, cxx, cpp, c++, h, hh, hxx, hpp, h++

.. option:: -d, --debug

    Enable additional debug output

.. option:: -h, --help

    Print help text summarizing the commandline options

Positional arguments
--------------------

.. option:: file

    Path to a single file which shall be checked.

    If file is ``-``, a list of files is read from standard input. This can be used for example with
    find to exclude specific files or directories::

        find -name "*.c" | grep -v tests | mcdc_checker.py -

Exit status
-----------

The following exit values are returned:

.. option:: 0

    No error occurred

.. option:: 1

    Generic error, see text output for further information

.. option:: 2

    Error found in checked files

.. option:: 3

    Invalid command line arguments
