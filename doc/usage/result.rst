..
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

Result Analysis
---------------

Running the MCDC Checker on one of the included test files produces the following output::

    Processing file mcdc-checker/tests/main.c
          ERROR: Invalid operator nesting! b || c [line=68, col=19]
      Non tree-like decision in file mcdc-checker/tests/main.c at line 2, column 9
      Decision in file mcdc-checker/tests/main.c in line 10, column 9 is tree-like
      Decision in file mcdc-checker/tests/main.c in line 18, column 9 is tree-like
      Decision in file mcdc-checker/tests/main.c in line 28, column 9 is tree-like
      Non tree-like decision in file mcdc-checker/tests/test.h at line 2, column 9
      Non tree-like decision in file mcdc-checker/tests/main.c at line 40, column 13
      Non tree-like decision in file mcdc-checker/tests/main.c at line 44, column 12
      Non tree-like decision in file mcdc-checker/tests/main.c at line 56, column 18
      Non tree-like decision in file mcdc-checker/tests/main.c at line 60, column 13
      Non tree-like decision in file mcdc-checker/tests/main.c at line 64, column 13
      Decision in file mcdc-checker/tests/main.c in line 68, column 13 is tree-like


    The following errors were found:
      No errors of type clang_parse_failed occurred
      No errors of type failed_to_create_bdd occurred
      invalid_operator_nesting occurred in:
        file mcdc-checker/tests/main.c in line 68 column 19
      No errors of type unexpected_node occurred
      bdd_is_not_tree_like occurred in:
        file mcdc-checker/tests/main.c in line 2 column 9
          Found solution: ['c', 'a', 'b']
        file mcdc-checker/tests/test.h in line 2 column 9
          Found solution: ['c', 'a', 'b']
        file mcdc-checker/tests/main.c in line 40 column 13
          Found solution: ['c', 'a', 'b']
        file mcdc-checker/tests/main.c in line 44 column 12
          Found solution: ['c', 'a', 'b']
        file mcdc-checker/tests/main.c in line 56 column 18
          Found solution: ['c', 'a', 'b']
        file mcdc-checker/tests/main.c in line 60 column 13
          Found solution: ['b < c', 'a < b', 'c > literal_3', 'b < literal_4']
        file mcdc-checker/tests/main.c in line 64 column 13
          Found solution: ['b', 'a', 'c']
      No errors of type bdd_is_not_tree_like_and_has_too_many_nodes occurred

During analysis, all errors emitted e.g. by the C/C++ preprocessor and errors found by the MCDC
checker are output as they are found. This is mostly useful for debug purposes and to tune the
define and include command line options to the MCDC checker.

After the analysis is done an error summary is generated, listing the location for each type of
error occurred. For MCDC coverage analysis, mostly the ``bdd_is_not_tree_like`` and
``bdd_is_not_tree_like_and_has_too_many_nodes`` error types are of interest. They indicate the same
problem, but for the latter, the tool will not try to find a solution as the computational overhead
is too large. In both cases it is required to refactor the source code in question to be able to
assess MCDC coverage with GCov.

If errors of other types have occurred, they either indicate very unusual coding style
(``invalid_operator_nesting``), missing includes or define directives (``clang_parse_failed``) or a
likely bug in the MCDC Checker (``failed_to_create_bdd`` and ``unexpected_node``).
