..
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

Installation
============

This tool can either be installed natively or to be able to
integrate it easily into CI pipelines, it is packaged as a docker image.

Arch Linux
----------

A native installation on Arch Linux is easily done by installing `mcdc-checker`
from the AUR, see https://aur.archlinux.org/packages/mcdc-checker.

Installation using pipx
-----------------------

MCDC Checker can be installed using `pipx`. First make sure that Clang 19 or
newer is installed on the system. Then run::

  pipx install mcdc-checker

Follow the instructions printed by `pipx` to be able to properly start MCDC
Checker by setting up your `PATH` variable. Then run MCDC Checker::

  mcdc-checker

Append any command line options for the MCDC Checker to that command according
to the :ref:`Usage` chapter.

Docker Image
------------

A docker image based on Arch Linux is available to use MCDC Checker in CI
environments. Install Docker according to your Linux distribution's manual,
then pull the MCDC Checker Docker image::

    docker pull registry.gitlab.com/gtd-gmbh/mcdc-checker/mcdc-checker

Alternatively the docker image can be built with::

    docker build -t mcdc-checker .

Then run a container using this image like so::

    docker run -it -v $(pwd):/code registry.gitlab.com/gtd-gmbh/mcdc-checker/mcdc-checker

Append any command line options for the MCDC Checker to that command according
to the :ref:`Usage` chapter.

Installation from Source
------------------------

Running MCDC Checker is possible from source. First make sure that Clang 19 or
newer and Poetry are installed. Then prepare a virtual environment using the
following commands inside of the MCDC Checker source directory::

  poetry install

Afterwards run the MCDC Checker using::

  poetry run mcdc-checker

Append any command line options for the MCDC Checker to that command according
to the :ref:`Usage` chapter.

This installation method is mostly useful for developing and maintaining MCDC
Checker.

CI/CD Integration
=================

Integration of the MCDC Checker into container based CI/CD infrastructures is easy. An example for
GitLab CI is provided below::

    mcdc-check:
      image:
        name: registry.gitlab.com/gtd-gmbh/mcdc-checker/mcdc-checker
        entrypoint: [""]
      script:
        - mcdc_checker -a -j report.json
      artifacts:
        reports:
          codequality: report.json

Add the necessary command line options according to the ``mcdc_checker`` command according to the :ref:`Usage` chapter.
