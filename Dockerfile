# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

FROM archlinux

RUN pacman -Syu --needed --noconfirm \
    python \
    python-pip \
    python-poetry \
    mypy \
    clang \
 && poetry config virtualenvs.in-project true \
 && yes | pacman -Scc
COPY . /opt/mcdc_checker
RUN cd /opt/mcdc_checker && poetry install
RUN ln -sf /opt/mcdc_checker/docker/mcdc_checker /usr/bin/

WORKDIR /code
ENTRYPOINT [ "mcdc_checker" ]
